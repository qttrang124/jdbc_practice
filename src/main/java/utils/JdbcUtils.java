package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author : TrangMNQ
 * @created : 9/7/2023, Thursday
 **/
public class JdbcUtils {
    public static Connection getConnection() {
        Connection c = null;
        try {
            //Đăng kí Driver Mysql với DriverManager
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());

            //Các tham số cần truyền
            String url = "jdbc:mysql://localhost:3306/books_collection";
            String username = "root";
            String password = "";

            //Tạo kết nối
            c = DriverManager.getConnection(url, username, password);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return c;
    }

    public static void closeConnection(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
