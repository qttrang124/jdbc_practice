import java.io.*;
import java.sql.*;

/**
 * @author : TrangMNQ
 * @created : 9/4/2023, Monday
 **/
public class JdbcExamples {
    public static void main(String[] args) {

        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());//optional

            String jdbcUrl = "jdbc:mysql://localhost:3306/books_collection";// truyền vào url tới db
            String username = "root";//chú ý username, password
            String pass = "";

            Connection connection = DriverManager.getConnection(jdbcUrl, username, pass);
            System.out.println("Connected to the database!");//test kết nối tới db

            Statement statement = connection.createStatement();
            String sqlQuery = "SELECT * FROM books";
            ResultSet resultSet = statement.executeQuery(sqlQuery);

            while (resultSet.next()) {
                // Process each row in the result set
                int id = resultSet.getInt("book_id");
                String name = resultSet.getString("book_name");
                // ... other columns
                System.out.println("ID: " + id + ", Name: " + name);
            }

            resultSet.close();
            statement.close();
            connection.close(); // sử dụng xong nhớ đóng kết nối
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
