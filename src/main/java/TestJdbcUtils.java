import dao.AuthorDao;
import dao.AuthorDaoImpl;
import models.Author;
import utils.JdbcUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * @author : TrangMNQ
 * @created : 9/7/2023, Thursday
 **/
public class TestJdbcUtils {
    public static void main(String[] args) {
        //b1
        Connection connection = JdbcUtils.getConnection();
        System.out.println("Success!!");


        //b2
        Author author = new Author(5, "Christian Brown", "Australia", new Date(77, 7, 23)
                , 350000000L);
//        saveAuthor(author);
        System.out.println(getAuthorByName(author.getAuthorName()));


        //b3

        //b5
        JdbcUtils.closeConnection(connection);
    }

    public static void saveAuthor(Author author) {
        AuthorDao authorDao = new AuthorDaoImpl();
        String result = authorDao.save(author);
        System.out.println("Save author: " + result);
    }

    public static List<Author> getAuthorByName(String name) {
        AuthorDao authorDao = new AuthorDaoImpl();
        List<Author> authorList = authorDao.getAuthorByNameLike(name);
        return authorList;
    }
}
