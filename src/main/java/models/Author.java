package models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.security.PrivilegedAction;
import java.sql.Date;

/**
 * @author : TrangMNQ
 * @created : 9/10/2023, Sunday
 **/

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class Author {
    private int authorId;
    private String authorName, nationality;
    private Date dateOfBirth;
    private long totalBookSale;

}
