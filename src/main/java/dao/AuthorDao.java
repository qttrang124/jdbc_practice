package dao;

import models.Author;

import java.util.List;

/**
 * @author : TrangMNQ
 * @created : 9/10/2023, Sunday
 **/
public interface AuthorDao {
    String save(Author author);

    List<Author> getAuthorByNameLike(String name);
}
