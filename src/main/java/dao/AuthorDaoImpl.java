package dao;

import models.Author;
import utils.JdbcUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author : TrangMNQ
 * @created : 9/10/2023, Sunday
 **/
public class AuthorDaoImpl implements AuthorDao {
    @Override
    public String save(Author author) {
        String result = "fail";
        int recordUpdated = 0;
        try {
            Connection connection = JdbcUtils.getConnection();
            String sql = "INSERT INTO author(author_id, author_name,nationality, date_of_birth, total_book_sales)" +
                    " VALUES (?,?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, author.getAuthorId());
            preparedStatement.setString(2, author.getAuthorName());
            preparedStatement.setString(3, author.getNationality());
            preparedStatement.setDate(4, author.getDateOfBirth());
            preparedStatement.setLong(5, author.getTotalBookSale());
            recordUpdated = preparedStatement.executeUpdate();


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if (recordUpdated > 0) {
            result = "success";
            return result;
        } else
            return result;
    }

    @Override
    public List<Author> getAuthorByNameLike(String name) {
        List<Author> result = new ArrayList<>();
        try {
            Connection connection = JdbcUtils.getConnection();
            String sql = "SELECT * FROM author where author_name like ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, "%Aga%");

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                result.add(new Author(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4),
                        rs.getInt(5)));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }
}
